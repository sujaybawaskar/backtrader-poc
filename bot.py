from __future__ import (absolute_import, division, print_function,
                        unicode_literals)
import backtrader as bt
import pytz

class St(bt.Strategy):
    def logdata(self):
        txt = []
        txt.append('{}'.format(len(self)))
        txt.append('{}'.format(self.data.datetime.datetime(0).isoformat()))
        # txt.append('EST={}'.format(self.data.datetime.datetime(0).astimezone(pytz.timezone('UTC')).isoformat()))
        txt.append('o={:.6f}'.format(self.data.open[0]))
        txt.append('h={:.6f}'.format(self.data.high[0]))
        txt.append('l={:.6f}'.format(self.data.low[0]))
        txt.append('c={:.6f}'.format(self.data.close[0]))
        txt.append('v={:.6f}'.format(self.data.volume[0]))
        # self.data.get(0, size=self.p.period)
        print(','.join(txt))

    data_live = False

    def notify_data(self, data, status, *args, **kwargs):
        print('*' * 5, 'DATA NOTIF:', data._getstatusname(status), *args)
        if status == data.LIVE:
            self.data_live = True

    def notify_order(self, order):
        buysell = 'BUY ' if order.isbuy() else 'SELL'
        print("shooting order type={}, status={},expected={}".format(buysell,order.status,order.Completed))
        if order.status == order.Completed:
            
            txt = '{} {}@{}'.format(buysell, order.executed.size,
                                    order.executed.price)
            print(txt)

    bought = 0
    sold = 0

    def next(self):
        self.logdata()
        
        if not self.data_live:
            return

        if not self.bought:
            self.bought = len(self)  # keep entry bar
            self.buy(size=100)
        elif not self.sold:
            if len(self) == (self.bought + 3):
                self.sell(size=100)


def run(args=None):
    cerebro = bt.Cerebro(stdstats=False)
    store = bt.stores.IBStore(port=7497)
    # instrument = "EUR.USD-CASH-IDEALPRO"
    instrument = "BANKNIFTY-IND-NSE-INR"
    # instrument = "TWTR"
    data = store.getdata(dataname=instrument, timeframe=bt.TimeFrame.Ticks)
    cerebro.resampledata(data, timeframe=bt.TimeFrame.Seconds, compression=30)

    cerebro.broker = store.getbroker()

    cerebro.addstrategy(St)
    cerebro.run()


if __name__ == '__main__':
    run()

