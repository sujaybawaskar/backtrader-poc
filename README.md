Environment Setup
Steps
-----------
1. python3.6 -m venv .env
2. python -m pip install -r requirements.txt


Backtrader home page : https://www.backtrader.com/
Backtrader Docs: https://www.backtrader.com/docu/
Backtrader GitHub : https://github.com/backtrader/backtrader
Backtrader Samples : https://github.com/backtrader/backtrader/tree/master/samples
