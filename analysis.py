from datetime import datetime
import backtrader as bt

class SmaCross(bt.SignalStrategy):
    # def __init__(self):
    #     sma1, sma2 = bt.ind.SMA(period=20), bt.ind.SMA(period=40)
    #     crossover = bt.ind.CrossOver(sma1, sma2)
    #     self.signal_add(bt.SIGNAL_LONG, crossover)
    def __init__(self):
        self.sma = bt.ind.SimpleMovingAverage(period=15)
        crossover = bt.ind.CrossOver(self.sma, self.data.close)
        self.signal_add(bt.SIGNAL_LONG, crossover)
        crossdown = bt.ind.CrossDown(self.sma, self.data.close)
        self.signal_add(bt.SIGNAL_SHORT, crossdown)
        

cerebro = bt.Cerebro()
cerebro.addstrategy(SmaCross)

data0 = bt.feeds.YahooFinanceData(dataname='INFY', fromdate=datetime(2018, 1, 1),
                                  todate=datetime(2019, 5, 2))
cerebro.adddata(data0)

cerebro.run()
cerebro.plot()