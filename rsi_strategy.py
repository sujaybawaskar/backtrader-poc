from datetime import datetime
import backtrader as bt
import pdb
class RSIStrategy(bt.SignalStrategy):
    def log(self, txt, dt=None):
        dt = dt or self.datas[0].datetime.date(0)
        print('%s, %s' % (dt.isoformat(), txt))

    def __init__(self):
        self.rsi = bt.ind.RSI(period=8)
        self.highest = bt.ind.MaxN(self.data,period=50) 
        self.lowest = bt.ind.MinN(self.data,period=50)
        self.bought = False
        self.order = None
        self.buyprice = None
        self.buycomm = None
        self.stop_loss = None
        self.take_profit = None
        self.lowest50 = None

    def notify_order(self, order):
        if order.status in [order.Submitted, order.Accepted]:
            return

        if order.status in [order.Completed]:
            if order.isbuy():
                self.log(
                    'BUY EXECUTED, Price: %.2f, Cost: %.2f, Comm %.2f' %
                    (order.executed.price,
                     order.executed.value,
                     order.executed.comm))
                self.buyprice = order.executed.price
                self.buycomm = order.executed.comm
                self.bought = True
            else:  # Sell
                self.bought= False
                self.log('SELL EXECUTED, Price: %.2f, Cost: %.2f, Comm %.2f' %
                         (order.executed.price,
                          order.executed.value,
                          order.executed.comm))

            self.bar_executed = len(self)

        elif order.status in [order.Canceled, order.Margin, order.Rejected]:
            self.log('Order status={}'.format(order.status))

        # Write down: no pending order
        self.order = None
    
    def notify_trade(self, trade):
        if not trade.isclosed:
            return

        self.log('OPERATION PROFIT, GROSS %.2f, NET %.2f' %
                 (trade.pnl, trade.pnlcomm))
    
    def next(self):
        if self.order:
            return
        close = self.data.close[0]
        high = self.data.high[0]
        low = self.data.low[0]
        # self.log("low={},self.lowest={}".format(low,self.lowest[0]))
        if self.rsi < 30 and low <= self.lowest[0]:
            # self.log("low={},self.lowest={}".format(low,self.lowest[0]))
            self.lowest50 = low

        if self.lowest50 == None:
            return

        
        
        risk = 10000
        short_signal = low < self.highest[-1] and self.rsi[-1] > 80
        long_signal = low > self.lowest50 and self.rsi[0] >20
        long_stop = self.lowest50 - (self.lowest50*0.02)
        size = risk/(close - long_stop)
        if long_signal and not self.bought:
            self.lowest50 = None
            # self.order = self.buy(size=100)
            long_tp = ((close - long_stop)*2) + close
            # print("long_stop={}, long_tp={}, size={}, close={},open={}, high={},lowest50={},rsi[-1]={},rsi[0]={}".format(long_stop,long_tp, size, close,self.data.open[0], high,self.lowest[-1],self.rsi[-1],self.rsi[0] ))
            brackets = self.buy_bracket(limitprice=long_tp, stopprice=long_stop,price = close, exectype=bt.Order.Limit, size= size)
            



          
cerebro = bt.Cerebro()
cerebro.addstrategy(RSIStrategy)
cerebro.broker.setcommission(commission=0.001)
cerebro.broker.setcash(10000000.0)
data0 = bt.feeds.YahooFinanceData(dataname='INFY.NS', fromdate=datetime(2017, 1, 1),
                                  todate=datetime(2019, 5, 2))
cerebro.adddata(data0)

cerebro.run()
cerebro.plot(style='candle')